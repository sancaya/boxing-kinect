﻿using UnityEngine;

namespace Assets.Game.Scripts.Models
{
    public class Health : MonoBehaviour
    {
        [Header("Logic Fuzzy Variable")]
        public AnimationCurve critical;
        public AnimationCurve hurt;
        public AnimationCurve healthy;
        [Header("Variables")]
        
        private float criticalValue = 0f;
        private float hurtValue = 0f;
        private float healthyValue = 0f;
        private float pDecicion = 0;
        
        public int Evaluate(float health)
        {

            healthyValue = healthy.Evaluate(health);
            hurtValue = hurt.Evaluate(health);
            criticalValue = critical.Evaluate(health);

            return VOutput(healthyValue,hurtValue,criticalValue);
        }

        public float GetPowerDecicion()
        {
            return pDecicion;
        }
        // 2 High, 1 Medium, 0 Low
        private int VOutput(float m1,float m2, float m3)
        {
            if (m1 >= m2)
            {
                pDecicion = m1 - (m2 + m3);
                return 2;
            }
            else if (m2 >= m3)
            {
                pDecicion = m2 - (m1 + m3);
                return 1;
            }
            else
            {
                pDecicion = m3 - (m2 + m1);
                return 0;
            }
        }

    }
}
