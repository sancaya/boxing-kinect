﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Models
{
    [System.Serializable]
    public class Character
    {
        [Header("Atribute")]
        public String name;
        public Material mat;
        public Sprite avatar;
        [Header("Core")]
        public float stamina;
        public float power;
        public float speed;

        float health = 1;
        public float getHealth()
        {
            return this.health;
        }

        public float getPower()
        {
            return this.power;
        }
        public float getStamina()
        {
            return this.stamina;
        }

        public float getSpeed()
        {
            return this.speed;
        }

    }
}
