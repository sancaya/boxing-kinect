﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager _instan;
        [Header("Contents")]
        public Models.Character[] characters;
        [Header("Settings")]
        public float criticalHit = 10;
        public int MaxRound = 12;
        public int CounTimeperRound = 180;

        Assets.Game.Scripts.Models.Character _player, _enemy;
        float hpPlayer, hpEnemy;
        float staPlayer, staEnemy;
        float powPlayer, powEnemy;
        Core.GameLogic _gameLogic;
        static bool _readyplay = false;

        bool play = false;
        bool overplay = false;

        public bool ready()
        {
            return _readyplay;
        }

        private void Awake()
        {
            if (_instan == null) {
                DontDestroyOnLoad(this);
                _instan = this;
            }
            else
            {
                Destroy(this);
            }
        }
        private void Start()
        {
            
        }

        #region GamePlay
        public void CriticalHit(Enum.CharacterEnum field,float crit)
        {
            switch (field){
                case Enum.CharacterEnum.Enemy :
                    if (popUpE||!play) return;
                    SetOnHit(field);
                    hpEnemy-=((criticalHit / 100)*(_player.getPower() * crit / 100));
                    break;
                case Enum.CharacterEnum.Player :
                    if (popUpE||!play) return;
                    SetOnHit(field);
                    hpPlayer-=((criticalHit / 100)*(_enemy.getPower() * crit / 100));
                    break;
            }
        }

        public void SetLogic(Core.GameLogic gameLogic,Kinect.PlayerController controller)
        {
            KinectManager.Instance.SetPlayerObjet(controller.gameObject);
            _gameLogic = gameLogic;
            _readyplay = true;
        }

        public bool IsPlayMode()
        {
            return play;
        }
        #endregion

        #region LoadScene
        public void PlayGame(Assets.Game.Scripts.Models.Character player, Assets.Game.Scripts.Models.Character enemy)
        {
            _player = player;
            _enemy = enemy;

            hpEnemy = enemy.getHealth();
            hpPlayer = player.getHealth();

            staEnemy = enemy.getStamina();
            staPlayer = player.getStamina();

            powEnemy = enemy.getPower();
            powPlayer = player.getPower();

            AudioManager._instan.StopMusic();
            play = true;
            StartCoroutine(LoadSceneGame(player,enemy));
        }

        public void GoToMenu()
        {
            KinectManager.Instance.PlayerObject = null;
            play = false;
            overplay = false;
            popUpE = false;
            _gameLogic = null;
            AudioManager._instan.StopMusic();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main_SelectCharacter");
        }

        IEnumerator LoadSceneGame(Assets.Game.Scripts.Models.Character player, Assets.Game.Scripts.Models.Character enemy)
        {
            
            AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Play");
            asyncOperation.allowSceneActivation = false;
            //When the load is still in progress, output the Text and progress bar
            while (!asyncOperation.isDone)
            {
                if (asyncOperation.progress >= 0.9f)
                {
                    asyncOperation.allowSceneActivation = true;
                }
                yield return null;
            }
        }
        #endregion

        #region Function Malicious
        public Assets.Game.Scripts.Models.Character GetCharPlayerData()
        {
            return _player;
        }

        public Assets.Game.Scripts.Models.Character GetCharEnemyData()
        {
            return _enemy;
        }

        public float PlayerHealth()
        {
            return hpPlayer;
        }
        public float EnemyHealth()
        {
            return hpEnemy;
        }

        public void GameKO(Enum.CharacterEnum e)
        {
            overplay = true;
        }
        public void GameDraw()
        {
            overplay = true;
        }
        public void SetOnHit(Enum.CharacterEnum e)
        {
            switch (e)
            {
                case Enum.CharacterEnum.Enemy:
                    staEnemy -= 2;
                    break;
                case Enum.CharacterEnum.Player:
                    staPlayer -= 2;
                    break;
            }
        }
        #endregion
        bool getUpE = false;
        bool popUpE = false;
        Enum.CharacterEnum spot = Enum.CharacterEnum.Player;
        private void Update()
        {
            #region PlayScene
            if (play)
            {
                if (hpEnemy < 1 && !(hpEnemy > 1) && staEnemy > 0)
                {
                    hpEnemy += (Time.deltaTime / 100);
                    staEnemy -= (Time.deltaTime / 100) * 0.01f;
                }
                if (hpPlayer < 1 && !(hpPlayer > 1) && staPlayer > 0)
                {
                    hpPlayer += (Time.deltaTime / 100);
                    staPlayer -= (Time.deltaTime / 100)*0.01f;
                }

                if (!popUpE)
                {
                    if (hpPlayer < 0)
                    {
                        popUpE = true;
                        Debug.Log("Play Mini Game");
                        spot = Enum.CharacterEnum.Player;
                        _gameLogic.EmptyHealth(spot);
                    }
                    else if (hpEnemy < 0)
                    {
                        popUpE = true;
                        Debug.Log("Play Mini Game");
                        spot = Enum.CharacterEnum.Enemy;
                        _gameLogic.EmptyHealth(spot);
                    }
                }
                else
                {
                    if (overplay) return;
                    if(spot == Enum.CharacterEnum.Player)
                    {
                        hpPlayer += (Time.deltaTime / 100) * staPlayer;
                        if (hpPlayer > 1)
                        {
                            popUpE = false;
                            _gameLogic.GetUpChar();
                        }
                    }
                    else
                    {
                        hpEnemy += (Time.deltaTime / 100) * staEnemy;
                        if (hpEnemy > 1)
                        {
                            popUpE = false;
                            _gameLogic.GetUpChar();
                        }
                    }
                    
                }
            }
            #endregion
        }
    }
}
