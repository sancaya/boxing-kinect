﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class Gloves : MonoBehaviour
    {
        public Enum.CharacterEnum charField = Enum.CharacterEnum.Player;
        public CriticalSpot[] allMyCriticalSpot;
        bool onStay = false;
        public Gloves otherGlove;
        private void Update()
        {
            if (Core.GameLogic._intant.OnNotready())
            {
                onStay = false;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            //if (charField == Enum.CharacterEnum.Player)
            //{
                if (other.transform.GetComponent<Gloves>() == null) return;
                if (other.transform.GetComponent<Gloves>().charField != charField)
                {
                    foreach (var item in allMyCriticalSpot)
                    {
                        item.OnDef = true;
                    }
                }
            //}
        }
        private void OnTriggerExit(Collider other)
        {
           //if (charField == Enum.CharacterEnum.Player)
            //{
                if (other.transform.GetComponent<Gloves>() == null) return;
                if (other.transform.GetComponent<Gloves>().charField != charField)
                {
                    Debug.Log("Exit " + other.transform.name);
                    foreach (var item in allMyCriticalSpot)
                    {
                        onStay = false;
                        if (otherGlove.onStay) return;
                        item.OnDef = false;
                    }
                }
            //}
        }
        private void OnTriggerStay(Collider other)
        {
            //if (charField == Enum.CharacterEnum.Player)
            //{
                if (other.transform.GetComponent<Gloves>() == null) return;
                if (other.transform.GetComponent<Gloves>().charField != charField)
                {
                    onStay = true;
                    Debug.Log("Stay " + other.transform.name);
                    foreach (var item in allMyCriticalSpot)
                    {
                        item.OnDef = true;
                    }
                }
            //}
        }
    }
}
