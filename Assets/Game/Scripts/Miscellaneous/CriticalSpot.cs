﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    
    public class CriticalSpot : MonoBehaviour
    {
        public Enum.CharacterEnum charField = Enum.CharacterEnum.Player;
        public bool OnDef = false;

        private void OnTriggerEnter(Collider other)
        {
            float a = OnDef ? 0.25f : 1f;
            if (other.GetComponent<Gloves>() == null) return;
            if (other.GetComponent<Gloves>().charField != charField)
            {
                if (GameManager._instan != null)
                {
                    AudioManager._instan.ForcePlaySFX(2);
                    GameManager._instan.CriticalHit(charField, a);
                }
            }
        }


    }
}
