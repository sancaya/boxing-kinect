﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class UIDetail : MonoBehaviour
    {
        [System.Serializable]
        public class MyObject
        {
            public string name;
            [Header("Set 1 - 100")]
            public float value;
        }
        public GameObject subPrefab;
        public MyObject[] Sub;

        public void Init()
        {
            foreach (var item in Sub)
            {
                if (subPrefab == null)
                {
                    Debug.LogError("UI Detail error, missing subPrefabs");
                    return;
                }
                GameObject clone = Instantiate(subPrefab, gameObject.transform);
                clone.transform.name = item.name;
                Transform[] childClone = clone.GetComponentsInChildren<Transform>();

                var text = childClone[1].GetComponent<UnityEngine.UI.Text>().text = item.name;
                var slider = childClone[2].GetComponent<UnityEngine.UI.Slider>().value = item.value/100;
            }
        }
    }
}
