﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class GameOverPopups : MonoBehaviour
    {
        public UnityEngine.UI.Text text;
        public UnityEngine.UI.Button btnBTM;
        public enum WinCondition
        {
            KO,LostPoint
        }
        public void MenuBTN()
        {
           GameManager._instan.GoToMenu();
        }

        public void WinSet(Enum.CharacterEnum character,WinCondition winCondition)
        {
            gameObject.SetActive(true);
            if(winCondition == WinCondition.KO)
            {
                switch (character)
                {
                    case Enum.CharacterEnum.Player:
                        AudioManager._instan.ForcePlaySFX(5);
                        SetTextV("You Lost (" + winCondition.ToString() + ")");
                        break;
                    case Enum.CharacterEnum.Enemy:
                        AudioManager._instan.ForcePlaySFX(4);
                        SetTextV("You Win ( With " + winCondition.ToString() + ")");
                        break;
                }
            }
            else
            {
                switch (character)
                {
                    case Enum.CharacterEnum.Player:
                        AudioManager._instan.ForcePlaySFX(5);
                        SetTextV("You Lost Point");
                        break;
                    case Enum.CharacterEnum.Enemy:
                        AudioManager._instan.ForcePlaySFX(4);
                        SetTextV("You Win Point");
                        break;
                }
            }
        }

        void SetTextV(string val)
        {
            text.text = val;
        }
    }
}
