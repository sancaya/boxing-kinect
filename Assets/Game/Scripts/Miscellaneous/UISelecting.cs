﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class UISelecting : MonoBehaviour
    {
        public UnityEngine.UI.Text EnemyName;
        public UnityEngine.UI.Text PlayerName;
        public UnityEngine.UI.Image EnemyAvt;
        public UnityEngine.UI.Image PlayerAvt;
    }
}
