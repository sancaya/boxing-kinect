﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class CloserDistanceEnemyHelper : MonoBehaviour
    {
        public static CloserDistanceEnemyHelper instance;

        public GameObject Gloves1;
        public GameObject Gloves2;

        float distance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
        public float getDist()
        {
            return distance;
        }
        private void Update()
        {
            float a = Vector3.Distance(gameObject.transform.position, Gloves1.transform.position);
            float b = Vector3.Distance(gameObject.transform.position, Gloves2.transform.position);
            if (a > b)
            {
                distance = b;
            }
            else
            {
                distance = a;
            }
        }
    }
}
