﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class CloserDistancePlayerHelper : MonoBehaviour
    {
        public static CloserDistancePlayerHelper instance;

        public GameObject Gloves1;//R
        public GameObject Gloves2;//L

        float distance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
        public float getDist()
        {
            return distance;
        }
        bool rB = false;
        public bool rightBetter()
        {
            return rB;
        }
        private void Update()
        {
            float a = Vector3.Distance(gameObject.transform.position, Gloves1.transform.position);
            float b = Vector3.Distance(gameObject.transform.position, Gloves2.transform.position);
            if (a > b)
            {
                distance = b;
                rB = true;
            }
            else
            {
                rB = false;
                distance = a;
            }

        }
    }
}
