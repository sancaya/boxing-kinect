﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class TimeCountBGame : MonoBehaviour
    {
        public UnityEngine.UI.Text numberText;
        public float countTime = 80;
        float _countTime;
        bool play = false;
        public bool GetOnCount()
        {
            return _countTime >= 0;
        }
        public void CountStart()
        {
            play = true;
            _countTime = countTime;
        }

        public void CountPause()
        {
            play = false;
        }
        public void CountResume()
        {
            play = true;
        }

        public bool isPause()
        {
            return play==false;
        }

        public void SetTime(float time)
        {
            countTime = time;
            _countTime = time;
            numberText.text = Mathf.RoundToInt(time).ToString();
        }
        public void Reset()
        {
            play = false;
            _countTime = countTime;
        }
        private void Start()
        {
            _countTime = countTime;
        }

        private void Update()
        {
            numberText.text = Mathf.RoundToInt(_countTime).ToString();
            if (play) _countTime -= Time.deltaTime;
            if (_countTime <= 0)
            {
                play = false;
            }
        }
    }
}
