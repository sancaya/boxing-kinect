﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class SFXHandler : MonoBehaviour
    {
        public AudioClip[] clip;

        List<AudioSource> audioSource;
        public AudioSource getObjectAudioSource(int a)
        {
            return audioSource[a];
        }
        public List<AudioSource> getAudioSources()
        {
            return audioSource;
        }
        private void Start()
        {
            audioSource = new List<AudioSource>();
            foreach (var item in clip)
            {
                GameObject ui = Instantiate(new GameObject() , this.gameObject.transform);
                AudioSource ad = ui.AddComponent<AudioSource>();
                ad.clip = item;
                ad.loop = false;
                ad.playOnAwake = false;
                audioSource.Add(ad);
            }
        }
    }
}
