﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class UIHeader : MonoBehaviour
    {
        GameManager gameManager;

        public GameObject left;
        public GameObject right;
        public GameObject center;

        //Variable
        UnityEngine.UI.Text _player, _enemy;
        UnityEngine.UI.Image _iplayer, _ienemy;
        UnityEngine.UI.Slider _splayer, _senemy;
        private void Awake()
        {
            init();
        }
        private void init()
        {
            _player = left.transform.Find("Name").GetComponent<UnityEngine.UI.Text>();
            _enemy = right.transform.Find("Name").GetComponent<UnityEngine.UI.Text>();

            _iplayer = left.transform.Find("Avatar").GetComponent<UnityEngine.UI.Image>();
            _ienemy = right.transform.Find("Avatar").GetComponent<UnityEngine.UI.Image>();

            _splayer = left.transform.Find("Health").GetComponent<UnityEngine.UI.Slider>();
            _senemy = right.transform.Find("Health").GetComponent<UnityEngine.UI.Slider>();

            if (GameManager._instan != null)
            {
                gameManager = GameManager._instan;

                SetNamePlayer(gameManager.GetCharPlayerData().name);
                SetNameEnemy(gameManager.GetCharEnemyData().name);

                SetAvatarPlayer(gameManager.GetCharPlayerData().avatar);
                SetAvatarEnemy(gameManager.GetCharEnemyData().avatar);
            }
        }
        private void LateUpdate()
        {
            if (GameManager._instan != null)
            {
                _splayer.value = gameManager.PlayerHealth();
                _senemy.value = gameManager.EnemyHealth();
            }
        }
        public void SetNamePlayer(string val)
        {
            _player.text = val;
        }

        public void SetNameEnemy(string val)
        {
            _enemy.text = val;
        }

        public void SetAvatarPlayer(UnityEngine.Sprite sprite)
        {
            _iplayer.sprite = sprite;
        }

        public void SetAvatarEnemy(UnityEngine.Sprite sprite)
        {
            _ienemy.sprite = sprite;
        }
    }
}
