﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Game.Scripts.Miscellaneous
{
    public class UIPlayer : MonoBehaviour
    {
        public GameObject prefabs;
        public Assets.Game.Scripts.Models.Character[] characters;

        private void Awake()
        {
            Init();
        }

        public void Init()
        {
            characters = GameObject.Find("Scripts").GetComponent<GameManager>().characters;
            foreach (var a in characters)
            {
                
                var chars = Instantiate(prefabs, gameObject.transform);
                chars.transform.name = a.name;

                UnityEngine.EventSystems.EventTrigger evTriger = chars.AddComponent<UnityEngine.EventSystems.EventTrigger>();

                UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
                entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerClick;
                entry.callback.AddListener((eventData) => {
                    FooSelected(a); 
                });

                evTriger.triggers.Add(entry);

                #region UpdateAtribute
                UnityEngine.UI.Text _name = chars.transform.Find("Name").GetComponent<UnityEngine.UI.Text>();
                if(_name != null) _name.text = a.name;

                UnityEngine.UI.Image avatar = chars.transform.Find("Avatar").GetComponent<UnityEngine.UI.Image>();
                if (avatar != null) avatar.sprite = a.avatar;

                UIDetail detail = chars.GetComponentInChildren<UIDetail>();
                detail.Sub = new Game.Scripts.Miscellaneous.UIDetail.MyObject[3];

                Game.Scripts.Miscellaneous.UIDetail.MyObject stamina = new Game.Scripts.Miscellaneous.UIDetail.MyObject();
                stamina.name = "Stamina";
                stamina.value = a.stamina;
                detail.Sub[0] = stamina;

                Game.Scripts.Miscellaneous.UIDetail.MyObject power = new Game.Scripts.Miscellaneous.UIDetail.MyObject();
                power.name = "Power";
                power.value = a.power;
                detail.Sub[1] = power;

                Game.Scripts.Miscellaneous.UIDetail.MyObject speed = new Game.Scripts.Miscellaneous.UIDetail.MyObject();
                speed.name = "Speed";
                speed.value = a.speed;
                detail.Sub[2] = speed;

                detail.Init();
                #endregion UpdateAtribute
            }
        }

        void FooSelected(Models.Character character)
        {
            Transform _Scripts = GameObject.Find("MainScripts").transform;
            _Scripts.GetComponent<UIManager>().EventSelectChar(character);
        }
    }
}
