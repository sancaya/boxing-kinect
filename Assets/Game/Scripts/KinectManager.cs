﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
using Assets.Game.Scripts.Kinect;

namespace Assets.Game.Scripts
{
    public class KinectManager : MonoBehaviour
    {
		public bool testScript = false;
		public enum Smoothing : int { None, Default, Medium, Aggressive }
		// Public Bool to determine whether to receive and compute the user map
		public bool ComputeUserMap = false;

		// Public Bool to determine whether to receive and compute the color map
		public bool ComputeColorMap = false;

		// Public Bool to determine whether to display user map on the GUI
		public bool DisplayUserMap = false;

		// Public Bool to determine whether to display color map on the GUI
		public bool DisplayColorMap = false;

		// Public Bool to determine whether to display the skeleton lines on user map
		public bool DisplaySkeletonLines = false;
		// Public Float to specify the image width used by depth and color maps, as % of the camera width. the height is calculated depending on the width.
		// if percent is zero, it is calculated internally to match the selected width and height of the depth image
		public float DisplayMapsWidthPercent = 20f;

		// How high off the ground is the sensor (in meters).
		public float SensorHeight = 1.0f;

		// Kinect elevation angle (in degrees)
		public int SensorAngle = 0;

		// Minimum user distance in order to process skeleton data
		public float MinUserDistance = 1.0f;

		// Maximum user distance, if any. 0 means no max-distance limitation
		public float MaxUserDistance = 0f;

		// Public Bool to determine whether to detect only the closest user or not
		public bool DetectClosestUser = true;

		// Public Bool to determine whether to use only the tracked joints (and ignore the inferred ones)
		public bool IgnoreInferredJoints = true;

		// Selection of smoothing parameters
		public Smoothing smoothing = Smoothing.Default;

		// Public Bool to determine the use of additional filters
		public bool UseBoneOrientationsFilter = false;
		public bool UseClippedLegsFilter = false;
		public bool UseBoneOrientationsConstraint = true;
		public bool UseSelfIntersectionConstraint = false;

		public GameObject PlayerObject;

		public Kinect.KinectGesture.Gestures PlayerCalibrationPose;

		public List<KinectGesture.Gestures> PlayerGestures;

		public float MinTimeBetweenGestures = 0.7f;

		public List<MonoBehaviour> GestureListeners;

		public UnityEngine.UI.Text CalibrationText;

		public GameObject HandCursor;

		public bool GesturesDebugText;

		// Bool to specify whether Left/Right-hand-cursor and the Click-gesture control the mouse cursor and click
		public bool ControlMouseCursor = false;

		private bool KinectInitialized = false;

		// Bools to keep track of who is currently calibrated.
		private bool PlayerCalibrated = false;

		private bool AllPlayersCalibrated = false;

		// Values to track which ID (assigned by the Kinect).
		private uint PlayerID;

		private int PlayerIndex;

		private Kinect.PlayerController playerController;

		// User Map vars.
		private Texture2D usersLblTex;
		private Color32[] usersMapColors;
		private ushort[] usersPrevState;
		private Rect usersMapRect;
		private int usersMapSize;

		private Texture2D usersClrTex;
		//Color[] usersClrColors;
		private Rect usersClrRect;

		//short[] usersLabelMap;
		private ushort[] usersDepthMap;
		private float[] usersHistogramMap;

		// List of all users
		private List<uint> allUsers;

		// Image stream handles for the kinect
		private IntPtr colorStreamHandle;
		private IntPtr depthStreamHandle;

		// Color image data, if used
		private Color32[] colorImage;
		private byte[] usersColorMap;

		// Skeleton related structures
		private Kinect.KinectWrapper.NuiSkeletonFrame skeletonFrame;
		private Kinect.KinectWrapper.NuiTransformSmoothParameters smoothParameters;
		private int playerIndex;

		// Skeleton tracking states, positions and joints' orientations
		private Vector3 playerPos;
		private Matrix4x4 playerOri;
		private bool[] playerJointsTracked;
		private bool[] playerPrevTracked;
		private Vector3[] playerJointsPos;
		private Matrix4x4[] playerJointsOri;
		private Kinect.KinectWrapper.NuiSkeletonBoneOrientation[] jointOrientations;

		private Kinect.KinectGesture.GestureData playerCalibrationData;

		// Lists of gesture data, for each player
		private List<Kinect.KinectGesture.GestureData> playerGestures = new List<Kinect.KinectGesture.GestureData>();

		private float[] gestureTrackingAtTime;

		public List<Kinect.KinectGesture.GestureListenerInterface> gestureListeners;

		private Matrix4x4 kinectToWorld, flipMatrix;
		private static KinectManager instance;

		private float lastNuiTime;

		// Filters
		private Kinect.Filters.TrackingStateFilter[] trackingStateFilter;
		private Kinect.Filters.BoneOrientationsFilter[] boneOrientationFilter;
		private Kinect.Filters.ClippedLegsFilter[] clippedLegsFilter;
		private Kinect.Filters.BoneOrientationsConstraint boneConstraintsFilter;
		private Kinect.Filters.SelfIntersectionConstraint selfIntersectionConstraint;

		// returns the single KinectManager instance
		public static KinectManager Instance
		{
			get
			{
				return instance;
			}
		}

		public static bool IsKinectInitialized()
		{
			return instance != null ? instance.KinectInitialized : false;
		}
		public bool IsInitialized()
		{
			return KinectInitialized;
		}
		public static bool IsCalibrationNeeded()
		{
			return false;
		}
		public ushort[] GetRawDepthMap()
		{
			return usersDepthMap;
		}
		public ushort GetDepthForPixel(int x, int y)
		{
			int index = y * KinectWrapper.Constants.DepthImageWidth + x;

			if (index >= 0 && index < usersDepthMap.Length)
				return usersDepthMap[index];
			else
				return 0;
		}
		public Vector2 GetDepthMapPosForJointPos(Vector3 posJoint)
		{
			Vector3 vDepthPos = KinectWrapper.MapSkeletonPointToDepthPoint(posJoint);
			Vector2 vMapPos = new Vector2(vDepthPos.x, vDepthPos.y);

			return vMapPos;
		}
		public Vector2 GetColorMapPosForDepthPos(Vector2 posDepth)
		{
			int cx, cy;

			KinectWrapper.NuiImageViewArea pcViewArea = new KinectWrapper.NuiImageViewArea
			{
				eDigitalZoom = 0,
				lCenterX = 0,
				lCenterY = 0
			};

			KinectWrapper.NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(
				KinectWrapper.Constants.ColorImageResolution,
				KinectWrapper.Constants.DepthImageResolution,
				ref pcViewArea,
				(int)posDepth.x, (int)posDepth.y, GetDepthForPixel((int)posDepth.x, (int)posDepth.y),
				out cx, out cy);

			return new Vector2(cx, cy);
		}

		public Texture2D GetUsersLblTex()
		{
			return usersLblTex;
		}
		public Texture2D GetUsersClrTex()
		{
			return usersClrTex;
		}
		public bool IsUserDetected()
		{
			return KinectInitialized && (allUsers.Count > 0);
		}
		public uint GetPlayerID()
		{
			return PlayerID;
		}

		public int GetPlayerIndex()
		{
			return PlayerIndex;
		}

		public bool IsPlayerCalibrated(uint UserId)
		{
			if (UserId == PlayerID)
				return PlayerCalibrated;

			return false;
		}

		public Vector3 GetRawSkeletonJointPos(uint UserId, int joint)
		{
			if (UserId == PlayerID)
				return joint >= 0 && joint < playerJointsPos.Length ? (Vector3)skeletonFrame.SkeletonData[playerIndex].SkeletonPositions[joint] : Vector3.zero;
			
			return Vector3.zero;
		}

		public Vector3 GetUserPosition(uint UserId)
		{
			if (UserId == PlayerID)
				return playerPos;

			return Vector3.zero;
		}

		public Quaternion GetUserOrientation(uint UserId, bool flip)
		{
			if (UserId == PlayerID && playerJointsTracked[(int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter])
				return ConvertMatrixToQuat(playerOri, (int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter, flip);
			return Quaternion.identity;
		}

		public bool IsJointTracked(uint UserId, int joint)
		{
			if (UserId == PlayerID)
				return joint >= 0 && joint < playerJointsTracked.Length ? playerJointsTracked[joint] : false;
			return false;
		}

		public Vector3 GetJointPosition(uint UserId, int joint)
		{
			if (UserId == PlayerID)
				return joint >= 0 && joint < playerJointsPos.Length ? playerJointsPos[joint] : Vector3.zero;
			return Vector3.zero;
		}
		public Vector3 GetJointLocalPosition(uint UserId, int joint)
		{
			int parent = KinectWrapper.GetSkeletonJointParent(joint);

			if (UserId == PlayerID)
				return joint >= 0 && joint < playerJointsPos.Length ?
					(playerJointsPos[joint] - playerJointsPos[parent]) : Vector3.zero;
			return Vector3.zero;
		}
		public Quaternion GetJointOrientation(uint UserId, int joint, bool flip)
		{
			if (UserId == PlayerID)
			{
				if (joint >= 0 && joint < playerJointsOri.Length && playerJointsTracked[joint])
					return ConvertMatrixToQuat(playerJointsOri[joint], joint, flip);
			}

			return Quaternion.identity;
		}
		public Quaternion GetJointLocalOrientation(uint UserId, int joint, bool flip)
		{
			int parent = KinectWrapper.GetSkeletonJointParent(joint);

			if (UserId == PlayerID)
			{
				if (joint >= 0 && joint < playerJointsOri.Length && playerJointsTracked[joint])
				{
					Matrix4x4 localMat = (playerJointsOri[parent].inverse * playerJointsOri[joint]);
					return Quaternion.LookRotation(localMat.GetColumn(2), localMat.GetColumn(1));
				}
			}

			return Quaternion.identity;
		}
		public Vector3 GetDirectionBetweenJoints(uint UserId, int baseJoint, int nextJoint, bool flipX, bool flipZ)
		{
			Vector3 jointDir = Vector3.zero;

			if (UserId == PlayerID)
			{
				if (baseJoint >= 0 && baseJoint < playerJointsPos.Length && playerJointsTracked[baseJoint] &&
					nextJoint >= 0 && nextJoint < playerJointsPos.Length && playerJointsTracked[nextJoint])
				{
					jointDir = playerJointsPos[nextJoint] - playerJointsPos[baseJoint];
				}
			}

			if (jointDir != Vector3.zero)
			{
				if (flipX)
					jointDir.x = -jointDir.x;

				if (flipZ)
					jointDir.z = -jointDir.z;
			}

			return jointDir;
		}
		public void SetPlayerObjet(GameObject _gameObject)
        {
			PlayerObject = _gameObject;
        }
		public void DetectGesture(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);
			if (index >= 0)
				DeleteGesture(UserId, gesture);

			Kinect.KinectGesture.GestureData gestureData = new Kinect.KinectGesture.GestureData();

			gestureData.userId = UserId;
			gestureData.gesture = gesture;
			gestureData.state = 0;
			gestureData.joint = 0;
			gestureData.progress = 0f;
			gestureData.complete = false;
			gestureData.cancelled = false;

			gestureData.checkForGestures = new List<Kinect.KinectGesture.Gestures>();
			switch (gesture)
			{
				case Kinect.KinectGesture.Gestures.ZoomIn:
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.ZoomOut);
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Wheel);
					break;

				case Kinect.KinectGesture.Gestures.ZoomOut:
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.ZoomIn);
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Wheel);
					break;

				case Kinect.KinectGesture.Gestures.Wheel:
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.ZoomIn);
					gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.ZoomOut);
					break;

					//			case Kinect.KinectGesture.Gestures.Jump:
					//				gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Squat);
					//				break;
					//				
					//			case Kinect.KinectGesture.Gestures.Squat:
					//				gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Jump);
					//				break;
					//				
					//			case Kinect.KinectGesture.Gestures.Push:
					//				gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Pull);
					//				break;
					//				
					//			case Kinect.KinectGesture.Gestures.Pull:
					//				gestureData.checkForGestures.Add(Kinect.KinectGesture.Gestures.Push);
					//				break;
			}

			if (UserId == PlayerID)
				playerGestures.Add(gestureData);
		}

		public bool ResetGesture(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);
			if (index < 0)
				return false;

			Kinect.KinectGesture.GestureData gestureData = playerGestures[index];

			gestureData.state = 0;
			gestureData.joint = 0;
			gestureData.progress = 0f;
			gestureData.complete = false;
			gestureData.cancelled = false;
			gestureData.startTrackingAtTime = Time.realtimeSinceStartup + Kinect.KinectWrapper.Constants.MinTimeBetweenSameGestures;

			if (UserId == PlayerID)
				playerGestures[index] = gestureData;

			return true;
		}

		public void ResetPlayerGestures(uint UserId)
		{
			if (UserId == PlayerID)
			{
				int listSize = playerGestures.Count;

				for (int i = 0; i < listSize; i++)
				{
					ResetGesture(UserId, playerGestures[i].gesture);
				}
			}
		}

		public bool DeleteGesture(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);
			if (index < 0)
				return false;

			if (UserId == PlayerID)
				playerGestures.RemoveAt(index);

			return true;
		}

		public void ClearGestures(uint UserId)
		{
			if (UserId == PlayerID)
			{
				playerGestures.Clear();
			}
		}

		public int GetGesturesCount(uint UserId)
		{
			if (UserId == PlayerID)
				return playerGestures.Count;

			return 0;
		}

		public List<Kinect.KinectGesture.Gestures> GetGesturesList(uint UserId)
		{
			List<Kinect.KinectGesture.Gestures> list = new List<Kinect.KinectGesture.Gestures>();

			if (UserId == PlayerID)
			{
				foreach (Kinect.KinectGesture.GestureData data in playerGestures)
					list.Add(data.gesture);
			}

			return list;
		}

		public bool IsGestureDetected(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);
			return index >= 0;
		}

		public bool IsGestureComplete(uint UserId, Kinect.KinectGesture.Gestures gesture, bool bResetOnComplete)
		{
			int index = GetGestureIndex(UserId, gesture);

			if (index >= 0)
			{
				if (UserId == PlayerID)
				{
					Kinect.KinectGesture.GestureData gestureData = playerGestures[index];

					if (bResetOnComplete && gestureData.complete)
					{
						ResetPlayerGestures(UserId);
						return true;
					}

					return gestureData.complete;
				}
			}

			return false;
		}
		public bool IsGestureCancelled(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);

			if (index >= 0)
			{
				if (UserId == PlayerID)
				{
					Kinect.KinectGesture.GestureData gestureData = playerGestures[index];
					return gestureData.cancelled;
				}
			}

			return false;
		}

		public float GetGestureProgress(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);

			if (index >= 0)
			{
				if (UserId == PlayerID)
				{
					Kinect.KinectGesture.GestureData gestureData = playerGestures[index];
					return gestureData.progress;
				}
			}

			return 0f;
		}

		public Vector3 GetGestureScreenPos(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			int index = GetGestureIndex(UserId, gesture);

			if (index >= 0)
			{
				if (UserId == PlayerID)
				{
					Kinect.KinectGesture.GestureData gestureData = playerGestures[index];
					return gestureData.screenPos;
				}
			}

			return Vector3.zero;
		}

		public void ResetGestureListeners()
		{
			// create the list of gesture listeners
			gestureListeners.Clear();

			foreach (MonoBehaviour script in GestureListeners)
			{
				if (script && (script is Kinect.KinectGesture.GestureListenerInterface))
				{
					Kinect.KinectGesture.GestureListenerInterface listener = (Kinect.KinectGesture.GestureListenerInterface)script;
					gestureListeners.Add(listener);
				}
			}

		}

		public void ResetAvatarControllers()
		{
			//if (PlayerAvatars.Count == 0)
			//{
			//	AvatarController[] avatars = FindObjectsOfType(typeof(AvatarController)) as AvatarController[];

			//	foreach (AvatarController avatar in avatars)
			//	{
			//		PlayerAvatars.Add(avatar.gameObject);
			//	}
			//}

			//if (PlayerController != null)
			//{
			//	PlayerController.Clear();

			//	foreach (GameObject avatar in PlayerAvatars)
			//	{
			//		if (avatar != null && avatar.activeInHierarchy)
			//		{
			//			AvatarController controller = avatar.GetComponent<AvatarController>();
			//			controller.ResetToInitialPosition();
			//			controller.Awake();

			//			PlayerControllers.Add(controller);
			//		}
			//	}
			//}

			Debug.Log("Reset Controller");
		}

		public void ClearKinectUsers()
		{
			if (!KinectInitialized)
				return;

			// remove current users
			for (int i = allUsers.Count - 1; i >= 0; i--)
			{
				uint userId = allUsers[i];
				RemoveUser(userId);
			}

			ResetFilters();
		}

		public void ResetFilters()
		{
			if (!KinectInitialized)
				return;

			// clear kinect vars
			playerPos = Vector3.zero;
			playerOri = Matrix4x4.identity;

			int skeletonJointsCount = (int)KinectWrapper.NuiSkeletonPositionIndex.Count;
			for (int i = 0; i < skeletonJointsCount; i++)
			{
				playerJointsTracked[i] = false;
				playerPrevTracked[i] = false; 
				playerJointsPos[i] = Vector3.zero; 
				playerJointsOri[i] = Matrix4x4.identity; 
			}

			if (trackingStateFilter != null)
			{
				for (int i = 0; i < trackingStateFilter.Length; i++)
					if (trackingStateFilter[i] != null)
						trackingStateFilter[i].Reset();
			}

			if (boneOrientationFilter != null)
			{
				for (int i = 0; i < boneOrientationFilter.Length; i++)
					if (boneOrientationFilter[i] != null)
						boneOrientationFilter[i].Reset();
			}

			if (clippedLegsFilter != null)
			{
				for (int i = 0; i < clippedLegsFilter.Length; i++)
					if (clippedLegsFilter[i] != null)
						clippedLegsFilter[i].Reset();
			}
		}

		//---------------------------------------------------------------//

		void Awake()
		{
			if (instance != null)
			{
				Destroy(this);
			};
			//CalibrationText = GameObject.Find("CalibrationText");
			int hr = 0;

			try
			{
				hr = Kinect.KinectWrapper.NuiInitialize(Kinect.KinectWrapper.NuiInitializeFlags.UsesSkeleton |
					Kinect.KinectWrapper.NuiInitializeFlags.UsesDepthAndPlayerIndex |
					(ComputeColorMap ? Kinect.KinectWrapper.NuiInitializeFlags.UsesColor : 0));
				if (hr != 0)
				{
					throw new Exception("NuiInitialize Failed");
				}

				hr = Kinect.KinectWrapper.NuiSkeletonTrackingEnable(IntPtr.Zero, 8);  // 0, 12,8
				if (hr != 0)
				{
					throw new Exception("Cannot initialize Skeleton Data");
				}

				depthStreamHandle = IntPtr.Zero;
				if (ComputeUserMap)
				{
					hr = Kinect.KinectWrapper.NuiImageStreamOpen(Kinect.KinectWrapper.NuiImageType.DepthAndPlayerIndex,
						Kinect.KinectWrapper.Constants.DepthImageResolution, 0, 2, IntPtr.Zero, ref depthStreamHandle);
					if (hr != 0)
					{
						throw new Exception("Cannot open depth stream");
					}
				}

				colorStreamHandle = IntPtr.Zero;
				if (ComputeColorMap)
				{
					hr = Kinect.KinectWrapper.NuiImageStreamOpen(Kinect.KinectWrapper.NuiImageType.Color,
						Kinect.KinectWrapper.Constants.ColorImageResolution, 0, 2, IntPtr.Zero, ref colorStreamHandle);
					if (hr != 0)
					{
						throw new Exception("Cannot open color stream");
					}
				}

				// set kinect elevation angle
				Kinect.KinectWrapper.NuiCameraElevationSetAngle(SensorAngle);

				// init skeleton structures
				skeletonFrame = new Kinect.KinectWrapper.NuiSkeletonFrame()
				{
					SkeletonData = new Kinect.KinectWrapper.NuiSkeletonData[Kinect.KinectWrapper.Constants.NuiSkeletonCount]
				};

				// values used to pass to smoothing function
				smoothParameters = new Kinect.KinectWrapper.NuiTransformSmoothParameters();

				switch (smoothing)
				{
					case Smoothing.Default:
						smoothParameters.fSmoothing = 0.5f;
						smoothParameters.fCorrection = 0.5f;
						smoothParameters.fPrediction = 0.5f;
						smoothParameters.fJitterRadius = 0.05f;
						smoothParameters.fMaxDeviationRadius = 0.04f;
						break;
					case Smoothing.Medium:
						smoothParameters.fSmoothing = 0.5f;
						smoothParameters.fCorrection = 0.1f;
						smoothParameters.fPrediction = 0.5f;
						smoothParameters.fJitterRadius = 0.1f;
						smoothParameters.fMaxDeviationRadius = 0.1f;
						break;
					case Smoothing.Aggressive:
						smoothParameters.fSmoothing = 0.7f;
						smoothParameters.fCorrection = 0.3f;
						smoothParameters.fPrediction = 1.0f;
						smoothParameters.fJitterRadius = 1.0f;
						smoothParameters.fMaxDeviationRadius = 1.0f;
						break;
				}

				// init the tracking state filter
				trackingStateFilter = new Kinect.Filters.TrackingStateFilter[Kinect.KinectWrapper.Constants.NuiSkeletonMaxTracked];
				for (int i = 0; i < trackingStateFilter.Length; i++)
				{
					trackingStateFilter[i] = new Kinect.Filters.TrackingStateFilter();
					trackingStateFilter[i].Init();
				}

				// init the bone orientation filter
				boneOrientationFilter = new Kinect.Filters.BoneOrientationsFilter[Kinect.KinectWrapper.Constants.NuiSkeletonMaxTracked];
				for (int i = 0; i < boneOrientationFilter.Length; i++)
				{
					boneOrientationFilter[i] = new Kinect.Filters.BoneOrientationsFilter();
					boneOrientationFilter[i].Init();
				}

				// init the clipped legs filter
				clippedLegsFilter = new Kinect.Filters.ClippedLegsFilter[Kinect.KinectWrapper.Constants.NuiSkeletonMaxTracked];
				for (int i = 0; i < clippedLegsFilter.Length; i++)
				{
					clippedLegsFilter[i] = new Kinect.Filters.ClippedLegsFilter();
				}

				// init the bone orientation constraints
				boneConstraintsFilter = new Kinect.Filters.BoneOrientationsConstraint();
				boneConstraintsFilter.AddDefaultConstraints();
				// init the self intersection constraints
				selfIntersectionConstraint = new Kinect.Filters.SelfIntersectionConstraint();

				// create arrays for joint positions and joint orientations
				int skeletonJointsCount = (int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.Count;

				playerJointsTracked = new bool[skeletonJointsCount];
				playerPrevTracked = new bool[skeletonJointsCount];

				playerJointsPos = new Vector3[skeletonJointsCount];

				playerJointsOri = new Matrix4x4[skeletonJointsCount];

				gestureTrackingAtTime = new float[Kinect.KinectWrapper.Constants.NuiSkeletonMaxTracked];

				//create the transform matrix that converts from kinect-space to world-space
				Quaternion quatTiltAngle = new Quaternion();
				quatTiltAngle.eulerAngles = new Vector3(-SensorAngle, 0.0f, 0.0f);

				//float heightAboveHips = SensorHeight - 1.0f;

				// transform matrix - kinect to world
				//kinectToWorld.SetTRS(new Vector3(0.0f, heightAboveHips, 0.0f), quatTiltAngle, Vector3.one);
				kinectToWorld.SetTRS(new Vector3(0.0f, SensorHeight, 0.0f), quatTiltAngle, Vector3.one);
				flipMatrix = Matrix4x4.identity;
				flipMatrix[2, 2] = -1;

				instance = this;
			}
			catch (DllNotFoundException e)
			{
				string message = "Please check the Kinect SDK installation.";
				Debug.LogError(message);
				Debug.LogError(e.ToString());
                if (CalibrationText != null)
                    CalibrationText.text = message;

                return;
			}
			catch (Exception e)
			{
				string message = e.Message + " - " + Kinect.KinectWrapper.GetNuiErrorString(hr);
				Debug.LogError(message);
				Debug.LogError(e.ToString());
                if (CalibrationText != null)
                    CalibrationText.text = message;

                return;
			}

			if (ComputeUserMap)
			{
				// Initialize depth & label map related stuff
				usersMapSize = Kinect.KinectWrapper.GetDepthWidth() * Kinect.KinectWrapper.GetDepthHeight();
				usersLblTex = new Texture2D(Kinect.KinectWrapper.GetDepthWidth(), Kinect.KinectWrapper.GetDepthHeight());
				usersMapColors = new Color32[usersMapSize];
				usersPrevState = new ushort[usersMapSize];

				usersDepthMap = new ushort[usersMapSize];
				usersHistogramMap = new float[8192];
			}

			if (ComputeColorMap)
			{
				// Initialize color map related stuff
				usersClrTex = new Texture2D(Kinect.KinectWrapper.GetColorWidth(), Kinect.KinectWrapper.GetColorHeight());

				colorImage = new Color32[Kinect.KinectWrapper.GetColorWidth() * Kinect.KinectWrapper.GetColorHeight()];
				usersColorMap = new byte[colorImage.Length << 2];
			}

			//// try to automatically find the available avatar controllers in the scene
			//if (Player1Avatars.Count == 0 && Player2Avatars.Count == 0)
			//{
			//	AvatarController[] avatars = FindObjectsOfType(typeof(AvatarController)) as AvatarController[];

			//	foreach (AvatarController avatar in avatars)
			//	{
			//		Player1Avatars.Add(avatar.gameObject);
			//	}
			//}

			// Initialize user list to contain ALL users.
			allUsers = new List<uint>();

			// Pull the AvatarController from each of the players Avatars.
			playerController = new Kinect.PlayerController();

			// Add each of the avatars' controllers into a list for each player.
			//foreach (GameObject avatar in PlayerAvatars)
			//{
			//	if (avatar != null && avatar.activeInHierarchy)
			//	{
			//		PlayerController.Add(avatar.GetComponent<AvatarController>());
			//	}
			//}

			// create the list of gesture listeners
			gestureListeners = new List<Kinect.KinectGesture.GestureListenerInterface>();

			foreach (MonoBehaviour script in GestureListeners)
			{
				if (script && (script is KinectGesture.GestureListenerInterface))
				{
					Kinect.KinectGesture.GestureListenerInterface listener = (Kinect.KinectGesture.GestureListenerInterface)script;
					gestureListeners.Add(listener);
				}
			}

            // GUI Text.
            if (CalibrationText != null)
            {
                CalibrationText.text = "WAITING FOR USERS";
            }

            Debug.Log("Waiting for users.");

			KinectInitialized = true;
		}
        private void Start()
        {
            if (KinectInitialized)
            {
				StartCoroutine(LoadSceneGame());
            }
        }

		IEnumerator LoadSceneGame()
		{

			AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Main_SelectCharacter");
			asyncOperation.allowSceneActivation = false;
			//When the load is still in progress, output the Text and progress bar
			while (!asyncOperation.isDone)
			{
				if (asyncOperation.progress >= 0.9f)
				{
					asyncOperation.allowSceneActivation = true;
				}
				yield return null;
			}
		}

		void Update()
		{
			if (KinectInitialized)
			{
				// needed by the KinectExtras' native wrapper to check for next frames
				// uncomment the line below, if you use the Extras' wrapper, but none of the Extras' managers
				//KinectWrapper.UpdateKinectSensor();

				// If the players aren't all calibrated yet, draw the user map.
				if (ComputeUserMap)
				{
					if (depthStreamHandle != IntPtr.Zero &&
						Kinect.KinectWrapper.PollDepth(depthStreamHandle, Kinect.KinectWrapper.Constants.IsNearMode, ref usersDepthMap))
					{
						UpdateUserMap();
					}
				}

				if (ComputeColorMap)
				{
					if (colorStreamHandle != IntPtr.Zero &&
						Kinect.KinectWrapper.PollColor(colorStreamHandle, ref usersColorMap, ref colorImage))
					{
						UpdateColorMap();
					}
				}

				if (Kinect.KinectWrapper.PollSkeleton(ref smoothParameters, ref skeletonFrame))
				{
					ProcessSkeleton();
				}

				// Update player 1's models if he/she is calibrated and the model is active.
				if (PlayerCalibrated)
				{

					if (PlayerObject == null) return;
					if (testScript)
					{
						playerController = PlayerObject.GetComponent<Kinect.PlayerController>();
						playerController.UpdateAvatar(PlayerID);
					}
					else if (GameManager._instan.ready())
					{
						playerController = PlayerObject.GetComponent<Kinect.PlayerController>();
						playerController.UpdateAvatar(PlayerID);
					}
					else return;

                    // Check for complete gestures
                    foreach (Kinect.KinectGesture.GestureData gestureData in playerGestures)
					{
						if (gestureData.complete)
						{
							if (gestureData.gesture == Kinect.KinectGesture.Gestures.Click)
							{
								if (ControlMouseCursor)
								{
									//MouseControl.MouseClick();
								}
							}

							foreach (Kinect.KinectGesture.GestureListenerInterface listener in gestureListeners)
							{
								if (listener.GestureCompleted(PlayerID, 0, gestureData.gesture,
															 (Kinect.KinectWrapper.NuiSkeletonPositionIndex)gestureData.joint, gestureData.screenPos))
								{
									ResetPlayerGestures(PlayerID);
								}
							}
						}
						else if (gestureData.cancelled)
						{
							foreach (Kinect.KinectGesture.GestureListenerInterface listener in gestureListeners)
							{
								if (listener.GestureCancelled(PlayerID, 0, gestureData.gesture,
															 (Kinect.KinectWrapper.NuiSkeletonPositionIndex)gestureData.joint))
								{
									ResetGesture(PlayerID, gestureData.gesture);
								}
							}
						}
						else if (gestureData.progress >= 0.1f)
						{
							if ((gestureData.gesture == Kinect.KinectGesture.Gestures.RightHandCursor ||
								gestureData.gesture == Kinect.KinectGesture.Gestures.LeftHandCursor) &&
								gestureData.progress >= 0.5f)
							{
								if (GetGestureProgress(gestureData.userId, Kinect.KinectGesture.Gestures.Click) < 0.3f)
								{
                                    if (HandCursor != null)
                                    {
                                        Vector3 vCursorPos = gestureData.screenPos;

                                        if (HandCursor == null)
                                        {
                                            //	//float zDist = HandCursor.transform.position.z - Camera.main.transform.position.z;


                                            vCursorPos = HandCursor.transform.position;
                                        }
										Debug.Log(vCursorPos);
										//                       HandCursor.transform.position = new Vector3(Vector3.Lerp(HandCursor.transform.position, vCursorPos, 3 * Time.deltaTime).x
										//, Vector3.Lerp(HandCursor.transform.position, vCursorPos, 3 * Time.deltaTime).y, 0);

										Vector3 a = new Vector3(HandCursor.transform.position.x, HandCursor.transform.position.y,0);
										Vector3 b = new Vector3(vCursorPos.x,vCursorPos.y,0);

										HandCursor.transform.position = Vector3.Lerp(a, b, 10 * Time.deltaTime);
										//                       Debug.Log(HandCursor.transform.position);
									}

                                    if (ControlMouseCursor)
                                    {
                                        Vector3 vCursorPos = HandCursor.transform.position;
                                        //MouseControl.MouseMove(vCursorPos, CalibrationText);
                                    }
                                }
							}

							foreach (Kinect.KinectGesture.GestureListenerInterface listener in gestureListeners)
							{
								listener.GestureInProgress(PlayerID, 0, gestureData.gesture, gestureData.progress,
														   (Kinect.KinectWrapper.NuiSkeletonPositionIndex)gestureData.joint, gestureData.screenPos);
							}
						}
					}
				}

			}

			// Kill the program with ESC.
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				Application.Quit();
			}
		}

		void OnApplicationQuit()
		{
			if (KinectInitialized)
			{
				// Shutdown OpenNI
				Kinect.KinectWrapper.NuiShutdown();
				instance = null;
			}
		}

		void OnGUI()
		{
			if (KinectInitialized)
			{
				if (ComputeUserMap && (/**(allUsers.Count == 0) ||*/ DisplayUserMap))
				{
					if (usersMapRect.width == 0 || usersMapRect.height == 0)
					{
						// get the main camera rectangle
						Rect cameraRect = Camera.main.pixelRect;

						// calculate map width and height in percent, if needed
						if (DisplayMapsWidthPercent == 0f)
						{
							DisplayMapsWidthPercent = (Kinect.KinectWrapper.GetDepthWidth() / 2) * 100 / cameraRect.width;
						}

						float displayMapsWidthPercent = DisplayMapsWidthPercent / 100f;
						float displayMapsHeightPercent = displayMapsWidthPercent * Kinect.KinectWrapper.GetDepthHeight() / Kinect.KinectWrapper.GetDepthWidth();

						float displayWidth = cameraRect.width * displayMapsWidthPercent;
						float displayHeight = cameraRect.width * displayMapsHeightPercent;

						usersMapRect = new Rect(cameraRect.width - displayWidth, cameraRect.height, displayWidth, -displayHeight);
					}

					GUI.DrawTexture(usersMapRect, usersLblTex);
				}

				else if (ComputeColorMap && (/**(allUsers.Count == 0) ||*/ DisplayColorMap))
				{
					if (usersClrRect.width == 0 || usersClrTex.height == 0)
					{
						// get the main camera rectangle
						Rect cameraRect = Camera.main.pixelRect;

						// calculate map width and height in percent, if needed
						if (DisplayMapsWidthPercent == 0f)
						{
							DisplayMapsWidthPercent = (Kinect.KinectWrapper.GetDepthWidth() / 2) * 100 / cameraRect.width;
						}

						float displayMapsWidthPercent = DisplayMapsWidthPercent / 100f;
						float displayMapsHeightPercent = displayMapsWidthPercent * Kinect.KinectWrapper.GetColorHeight() / Kinect.KinectWrapper.GetColorWidth();

						float displayWidth = cameraRect.width * displayMapsWidthPercent;
						float displayHeight = cameraRect.width * displayMapsHeightPercent;

						usersClrRect = new Rect(cameraRect.width - displayWidth, cameraRect.height, displayWidth, -displayHeight);

						//					if(ComputeUserMap)
						//					{
						//						usersMapRect.x -= cameraRect.width * DisplayMapsWidthPercent; //usersClrTex.width / 2;
						//					}
					}

					GUI.DrawTexture(usersClrRect, usersClrTex);
				}
			}
		}

		void UpdateUserMap()
		{
			int numOfPoints = 0;
			Array.Clear(usersHistogramMap, 0, usersHistogramMap.Length);

			// Calculate cumulative histogram for depth
			for (int i = 0; i < usersMapSize; i++)
			{
				// Only calculate for depth that contains users
				if ((usersDepthMap[i] & 7) != 0)
				{
					ushort userDepth = (ushort)(usersDepthMap[i] >> 3);
					usersHistogramMap[userDepth]++;
					numOfPoints++;
				}
			}

			if (numOfPoints > 0)
			{
				for (int i = 1; i < usersHistogramMap.Length; i++)
				{
					usersHistogramMap[i] += usersHistogramMap[i - 1];
				}

				for (int i = 0; i < usersHistogramMap.Length; i++)
				{
					usersHistogramMap[i] = 1.0f - (usersHistogramMap[i] / numOfPoints);
				}
			}

			// dummy structure needed by the coordinate mapper
			Kinect.KinectWrapper.NuiImageViewArea pcViewArea = new Kinect.KinectWrapper.NuiImageViewArea
			{
				eDigitalZoom = 0,
				lCenterX = 0,
				lCenterY = 0
			};

			// Create the actual users texture based on label map and depth histogram
			Color32 clrClear = Color.clear;
			for (int i = 0; i < usersMapSize; i++)
			{
				// Flip the texture as we convert label map to color array
				int flipIndex = i; // usersMapSize - i - 1;

				ushort userMap = (ushort)(usersDepthMap[i] & 7);
				ushort userDepth = (ushort)(usersDepthMap[i] >> 3);

				ushort nowUserPixel = userMap != 0 ? (ushort)((userMap << 13) | userDepth) : userDepth;
				ushort wasUserPixel = usersPrevState[flipIndex];

				// draw only the changed pixels
				if (nowUserPixel != wasUserPixel)
				{
					usersPrevState[flipIndex] = nowUserPixel;

					if (userMap == 0)
					{
						usersMapColors[flipIndex] = clrClear;
					}
					else
					{
						if (colorImage != null)
						{
							int x = i % Kinect.KinectWrapper.Constants.DepthImageWidth;
							int y = i / Kinect.KinectWrapper.Constants.DepthImageWidth;

							int cx, cy;
							int hr = Kinect.KinectWrapper.NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(
								Kinect.KinectWrapper.Constants.ColorImageResolution,
								Kinect.KinectWrapper.Constants.DepthImageResolution,
								ref pcViewArea,
								x, y, usersDepthMap[i],
								out cx, out cy);

							if (hr == 0)
							{
								int colorIndex = cx + cy * Kinect.KinectWrapper.Constants.ColorImageWidth;
								//colorIndex = usersMapSize - colorIndex - 1;
								if (colorIndex >= 0 && colorIndex < usersMapSize)
								{
									Color32 colorPixel = colorImage[colorIndex];
									usersMapColors[flipIndex] = colorPixel;  // new Color(colorPixel.r / 256f, colorPixel.g / 256f, colorPixel.b / 256f, 0.9f);
									usersMapColors[flipIndex].a = 230; // 0.9f
								}
							}
						}
						else
						{
							// Create a blending color based on the depth histogram
							float histDepth = usersHistogramMap[userDepth];
							Color c = new Color(histDepth, histDepth, histDepth, 0.9f);

							switch (userMap % 4)
							{
								case 0:
									usersMapColors[flipIndex] = Color.red * c;
									break;
								case 1:
									usersMapColors[flipIndex] = Color.green * c;
									break;
								case 2:
									usersMapColors[flipIndex] = Color.blue * c;
									break;
								case 3:
									usersMapColors[flipIndex] = Color.magenta * c;
									break;
							}
						}
					}

				}
			}

			// Draw it!
			usersLblTex.SetPixels32(usersMapColors);

			if (!DisplaySkeletonLines)
			{
				usersLblTex.Apply();
			}
		}

		void UpdateColorMap()
		{
			usersClrTex.SetPixels32(colorImage);
			usersClrTex.Apply();
		}

		void CalibrateUser(uint UserId, int UserIndex, ref Kinect.KinectWrapper.NuiSkeletonData skeletonData)
		{
			// If player 1 hasn't been calibrated, assign that UserID to it.
			if (!PlayerCalibrated)
			{
				// Check to make sure we don't accidentally assign player 2 to player 1.
				if (!allUsers.Contains(UserId))
				{
					if (CheckForCalibrationPose(UserId, ref PlayerCalibrationPose, ref playerCalibrationData, ref skeletonData))
					{
						PlayerCalibrated = true;
						PlayerID = UserId;
						PlayerIndex = UserIndex;

						allUsers.Add(UserId);

                        playerController.SuccessfulCalibration(UserId);

						// add the gestures to detect, if any
						foreach (KinectGesture.Gestures gesture in PlayerGestures)
						{
							DetectGesture(UserId, gesture);
						}

						// notify the gesture listeners about the new user
						foreach (Kinect.KinectGesture.GestureListenerInterface listener in gestureListeners)
						{
							listener.UserDetected(UserId, 0);
						}

						// reset skeleton filters
						ResetFilters();

						AllPlayersCalibrated = true; // true;
					}
				}
			}
			// If all users are calibrated, stop trying to find them.
			if (AllPlayersCalibrated)
			{
				Debug.Log("All players calibrated.");
			}
		}
		void RemoveUser(uint UserId)
		{
			// If we lose player 1...
			if (UserId == PlayerID)
			{
				// Null out the ID and reset all the models associated with that ID.
				PlayerID = 0;
				PlayerIndex = 0;
				PlayerCalibrated = false;

				playerController.ResetToInitialPosition();

				foreach (Kinect.KinectGesture.GestureListenerInterface listener in gestureListeners)
				{
					listener.UserLost(UserId, 0);
				}

				playerCalibrationData.userId = 0;
			}

			// clear gestures list for this user
			ClearGestures(UserId);

			// remove from global users list
			allUsers.Remove(UserId);
			AllPlayersCalibrated = false;

			// Try to replace that user!
			Debug.Log("Waiting for users.");
		}

		private const int stateTracked = (int)Kinect.KinectWrapper.NuiSkeletonPositionTrackingState.Tracked;
		private const int stateNotTracked = (int)Kinect.KinectWrapper.NuiSkeletonPositionTrackingState.NotTracked;

		private int[] mustBeTrackedJoints = {
		(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.AnkleLeft,
		(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.FootLeft,
		(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.AnkleRight,
		(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.FootRight,
		};

		void ProcessSkeleton()
		{
			List<uint> lostUsers = new List<uint>();
			lostUsers.AddRange(allUsers);

			// calculate the time since last update
			float currentNuiTime = Time.realtimeSinceStartup;
			float deltaNuiTime = currentNuiTime - lastNuiTime;

			for (int i = 0; i < Kinect.KinectWrapper.Constants.NuiSkeletonCount; i++)
			{
				Kinect.KinectWrapper.NuiSkeletonData skeletonData = skeletonFrame.SkeletonData[i];
				uint userId = skeletonData.dwTrackingID;

				if (skeletonData.eTrackingState == Kinect.KinectWrapper.NuiSkeletonTrackingState.SkeletonTracked)
				{
					// get the skeleton position
					Vector3 skeletonPos = kinectToWorld.MultiplyPoint3x4(skeletonData.Position);

					if (!AllPlayersCalibrated)
					{
						// check if this is the closest user
						bool bClosestUser = true;

						if (DetectClosestUser)
						{
							for (int j = 0; j < Kinect.KinectWrapper.Constants.NuiSkeletonCount; j++)
							{
								if (j != i)
								{
									Kinect.KinectWrapper.NuiSkeletonData skeletonDataOther = skeletonFrame.SkeletonData[j];

									if ((skeletonDataOther.eTrackingState == Kinect.KinectWrapper.NuiSkeletonTrackingState.SkeletonTracked) &&
										(Mathf.Abs(kinectToWorld.MultiplyPoint3x4(skeletonDataOther.Position).z) < Mathf.Abs(skeletonPos.z)))
									{
										bClosestUser = false;
										break;
									}
								}
							}
						}

						if (bClosestUser)
						{
							CalibrateUser(userId, i + 1, ref skeletonData);
						}
					}

					//// get joints orientations
					//KinectWrapper.NuiSkeletonBoneOrientation[] jointOrients = new KinectWrapper.NuiSkeletonBoneOrientation[(int)KinectWrapper.NuiSkeletonPositionIndex.Count];
					//KinectWrapper.NuiSkeletonCalculateBoneOrientations(ref skeletonData, jointOrients);

					if (userId == PlayerID && Mathf.Abs(skeletonPos.z) >= MinUserDistance &&
					   (MaxUserDistance <= 0f || Mathf.Abs(skeletonPos.z) <= MaxUserDistance))
					{
						playerIndex = i;

						// get player position
						playerPos = skeletonPos;

						// apply tracking state filter first
						trackingStateFilter[0].UpdateFilter(ref skeletonData);

						// fixup skeleton to improve avatar appearance.
						if (UseClippedLegsFilter && clippedLegsFilter[0] != null)
						{
							clippedLegsFilter[0].FilterSkeleton(ref skeletonData, deltaNuiTime);
						}

						if (UseSelfIntersectionConstraint && selfIntersectionConstraint != null)
						{
							selfIntersectionConstraint.Constrain(ref skeletonData);
						}

						// get joints' position and rotation
						for (int j = 0; j < (int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.Count; j++)
						{
							bool playerTracked = IgnoreInferredJoints ? (int)skeletonData.eSkeletonPositionTrackingState[j] == stateTracked :
								(Array.BinarySearch(mustBeTrackedJoints, j) >= 0 ? (int)skeletonData.eSkeletonPositionTrackingState[j] == stateTracked :
								(int)skeletonData.eSkeletonPositionTrackingState[j] != stateNotTracked);
							playerJointsTracked[j] = playerPrevTracked[j] && playerTracked;
							playerPrevTracked[j] = playerTracked;

							if (playerJointsTracked[j])
							{
								playerJointsPos[j] = kinectToWorld.MultiplyPoint3x4(skeletonData.SkeletonPositions[j]);
								//player1JointsOri[j] = jointOrients[j].absoluteRotation.rotationMatrix * flipMatrix;
							}

							//						if(j == (int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter)
							//						{
							//							string debugText = String.Format("{0} {1}", /**(int)skeletonData.eSkeletonPositionTrackingState[j], */
							//								player1JointsTracked[j] ? "T" : "F", player1JointsPos[j]/**, skeletonData.SkeletonPositions[j]*/);
							//							
							//							if(CalibrationText)
							//								CalibrationText.guiText.text = debugText;
							//						}
						}

						// draw the skeleton on top of texture
						if (DisplaySkeletonLines && ComputeUserMap)
						{
							DrawSkeleton(usersLblTex, ref skeletonData, ref playerJointsTracked);
							usersLblTex.Apply();
						}

						// calculate joint orientations
						KinectWrapper.GetSkeletonJointOrientation(ref playerJointsPos, ref playerJointsTracked, ref playerJointsOri);

						// filter orientation constraints
						if (UseBoneOrientationsConstraint && boneConstraintsFilter != null)
						{
							boneConstraintsFilter.Constrain(ref playerJointsOri, ref playerJointsTracked);
						}

						// filter joint orientations.
						// it should be performed after all joint position modifications.
						if (UseBoneOrientationsFilter && boneOrientationFilter[0] != null)
						{
							boneOrientationFilter[0].UpdateFilter(ref skeletonData, ref playerJointsOri);
						}

						// get player rotation
						playerOri = playerJointsOri[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipCenter];

						// check for gestures
						if (Time.realtimeSinceStartup >= gestureTrackingAtTime[0])
						{
							int listGestureSize = playerGestures.Count;
							float timestampNow = Time.realtimeSinceStartup;
							string sDebugGestures = string.Empty;  // "Tracked Gestures:\n";

							for (int g = 0; g < listGestureSize; g++)
							{
								Kinect.KinectGesture.GestureData gestureData = playerGestures[g];

								if ((timestampNow >= gestureData.startTrackingAtTime) &&
									!IsConflictingGestureInProgress(gestureData))
								{
									Kinect.KinectGesture.CheckForGesture(userId, ref gestureData, Time.realtimeSinceStartup,
										ref playerJointsPos, ref playerJointsTracked);
									playerGestures[g] = gestureData;

									if (gestureData.complete)
									{
										gestureTrackingAtTime[0] = timestampNow + MinTimeBetweenGestures;
									}

									//if(gestureData.state > 0)
									{
										sDebugGestures += string.Format("{0} - state: {1}, time: {2:F1}, progress: {3}%\n",
																		gestureData.gesture, gestureData.state,
																		gestureData.timestamp,
																		(int)(gestureData.progress * 100 + 0.5f));
									}
								}
							}

							if (GesturesDebugText)
							{
								sDebugGestures += string.Format("\n HandLeft: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HandLeft] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HandLeft].ToString() : "");
								sDebugGestures += string.Format("\n HandRight: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HandRight] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HandRight].ToString() : "");
								sDebugGestures += string.Format("\n ElbowLeft: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ElbowLeft] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ElbowLeft].ToString() : "");
								sDebugGestures += string.Format("\n ElbowRight: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ElbowRight] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ElbowRight].ToString() : "");

								sDebugGestures += string.Format("\n ShoulderLeft: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft].ToString() : "");
								sDebugGestures += string.Format("\n ShoulderRight: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight].ToString() : "");

								sDebugGestures += string.Format("\n Neck: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter].ToString() : "");
								sDebugGestures += string.Format("\n Hips: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipCenter] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipCenter].ToString() : "");
								sDebugGestures += string.Format("\n HipLeft: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipLeft] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipLeft].ToString() : "");
								sDebugGestures += string.Format("\n HipRight: {0}", playerJointsTracked[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipRight] ?
																playerJointsPos[(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.HipRight].ToString() : "");

								//GesturesDebugText.text = sDebugGestures;
								Debug.Log(sDebugGestures);
							}
						}
					}
					lostUsers.Remove(userId);
				}
			}

			// update the nui-timer
			lastNuiTime = currentNuiTime;

			// remove the lost users if any
			if (lostUsers.Count > 0)
			{
				foreach (uint userId in lostUsers)
				{
					RemoveUser(userId);
				}

				lostUsers.Clear();
			}
		}

		private void DrawSkeleton(Texture2D aTexture, ref Kinect.KinectWrapper.NuiSkeletonData skeletonData, ref bool[] playerJointsTracked)
		{
			int jointsCount = (int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.Count;

			for (int i = 0; i < jointsCount; i++)
			{
				int parent = Kinect.KinectWrapper.GetSkeletonJointParent(i);

				if (playerJointsTracked[i] && playerJointsTracked[parent])
				{
					Vector3 posParent = Kinect.KinectWrapper.MapSkeletonPointToDepthPoint(skeletonData.SkeletonPositions[parent]);
					Vector3 posJoint = Kinect.KinectWrapper.MapSkeletonPointToDepthPoint(skeletonData.SkeletonPositions[i]);

					//				posParent.y = KinectWrapper.Constants.ImageHeight - posParent.y - 1;
					//				posJoint.y = KinectWrapper.Constants.ImageHeight - posJoint.y - 1;
					//				posParent.x = KinectWrapper.Constants.ImageWidth - posParent.x - 1;
					//				posJoint.x = KinectWrapper.Constants.ImageWidth - posJoint.x - 1;

					//Color lineColor = playerJointsTracked[i] && playerJointsTracked[parent] ? Color.red : Color.yellow;
					DrawLine(aTexture, (int)posParent.x, (int)posParent.y, (int)posJoint.x, (int)posJoint.y, Color.yellow);
				}
			}
		}

		private void DrawLine(Texture2D a_Texture, int x1, int y1, int x2, int y2, Color a_Color)
		{
			int width = a_Texture.width;  // KinectWrapper.Constants.DepthImageWidth;
			int height = a_Texture.height;  // KinectWrapper.Constants.DepthImageHeight;

			int dy = y2 - y1;
			int dx = x2 - x1;

			int stepy = 1;
			if (dy < 0)
			{
				dy = -dy;
				stepy = -1;
			}

			int stepx = 1;
			if (dx < 0)
			{
				dx = -dx;
				stepx = -1;
			}

			dy <<= 1;
			dx <<= 1;

			if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
				for (int x = -1; x <= 1; x++)
					for (int y = -1; y <= 1; y++)
						a_Texture.SetPixel(x1 + x, y1 + y, a_Color);

			if (dx > dy)
			{
				int fraction = dy - (dx >> 1);

				while (x1 != x2)
				{
					if (fraction >= 0)
					{
						y1 += stepy;
						fraction -= dx;
					}

					x1 += stepx;
					fraction += dy;

					if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
						for (int x = -1; x <= 1; x++)
							for (int y = -1; y <= 1; y++)
								a_Texture.SetPixel(x1 + x, y1 + y, a_Color);
				}
			}
			else
			{
				int fraction = dx - (dy >> 1);

				while (y1 != y2)
				{
					if (fraction >= 0)
					{
						x1 += stepx;
						fraction -= dy;
					}

					y1 += stepy;
					fraction += dx;

					if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
						for (int x = -1; x <= 1; x++)
							for (int y = -1; y <= 1; y++)
								a_Texture.SetPixel(x1 + x, y1 + y, a_Color);
				}
			}

		}
		private Quaternion ConvertMatrixToQuat(Matrix4x4 mOrient, int joint, bool flip)
		{
			Vector4 vZ = mOrient.GetColumn(2);
			Vector4 vY = mOrient.GetColumn(1);

			if (!flip)
			{
				vZ.y = -vZ.y;
				vY.x = -vY.x;
				vY.z = -vY.z;
			}
			else
			{
				vZ.x = -vZ.x;
				vZ.y = -vZ.y;
				vY.z = -vY.z;
			}

			if (vZ.x != 0.0f || vZ.y != 0.0f || vZ.z != 0.0f)
				return Quaternion.LookRotation(vZ, vY);
			else
				return Quaternion.identity;
		}

		private int GetGestureIndex(uint UserId, Kinect.KinectGesture.Gestures gesture)
		{
			if (UserId == PlayerID)
			{
				int listSize = playerGestures.Count;
				for (int i = 0; i < listSize; i++)
				{
					if (playerGestures[i].gesture == gesture)
						return i;
				}
			}

			return -1;
		}

		private bool IsConflictingGestureInProgress(Kinect.KinectGesture.GestureData gestureData)
		{
			foreach (Kinect.KinectGesture.Gestures gesture in gestureData.checkForGestures)
			{
				int index = GetGestureIndex(gestureData.userId, gesture);

				if (index >= 0)
				{
					if (gestureData.userId == PlayerID)
					{
						if (playerGestures[index].progress > 0f)
							return true;
					}
				}
			}

			return false;
		}

		private bool CheckForCalibrationPose(uint userId, ref Kinect.KinectGesture.Gestures calibrationGesture,
		ref Kinect.KinectGesture.GestureData gestureData, ref Kinect.KinectWrapper.NuiSkeletonData skeletonData)
		{
			if (calibrationGesture == Kinect.KinectGesture.Gestures.None)
				return true;

			// init gesture data if needed
			if (gestureData.userId != userId)
			{
				gestureData.userId = userId;
				gestureData.gesture = calibrationGesture;
				gestureData.state = 0;
				gestureData.joint = 0;
				gestureData.progress = 0f;
				gestureData.complete = false;
				gestureData.cancelled = false;
			}

			// get temporary joints' position
			int skeletonJointsCount = (int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.Count;
			bool[] jointsTracked = new bool[skeletonJointsCount];
			Vector3[] jointsPos = new Vector3[skeletonJointsCount];

			int stateTracked = (int)Kinect.KinectWrapper.NuiSkeletonPositionTrackingState.Tracked;
			int stateNotTracked = (int)Kinect.KinectWrapper.NuiSkeletonPositionTrackingState.NotTracked;

			int[] mustBeTrackedJoints = {
			(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.AnkleLeft,
			(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.FootLeft,
			(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.AnkleRight,
			(int)Kinect.KinectWrapper.NuiSkeletonPositionIndex.FootRight,
		};

			for (int j = 0; j < skeletonJointsCount; j++)
			{
				jointsTracked[j] = Array.BinarySearch(mustBeTrackedJoints, j) >= 0 ? (int)skeletonData.eSkeletonPositionTrackingState[j] == stateTracked :
					(int)skeletonData.eSkeletonPositionTrackingState[j] != stateNotTracked;

				if (jointsTracked[j])
				{
					jointsPos[j] = kinectToWorld.MultiplyPoint3x4(skeletonData.SkeletonPositions[j]);
				}
			}

			// estimate the gesture progess
			Kinect.KinectGesture.CheckForGesture(userId, ref gestureData, Time.realtimeSinceStartup,
				ref jointsPos, ref jointsTracked);

			// check if gesture is complete
			if (gestureData.complete)
			{
				gestureData.userId = 0;
				return true;
			}

			return false;
		}

	}
}
