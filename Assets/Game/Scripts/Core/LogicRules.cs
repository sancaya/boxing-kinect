﻿using UnityEngine;

namespace Assets.Game.Scripts.Core
{
    public class LogicRules : MonoBehaviour
    {
        float hPlayer, hNPC;
        Assets.Game.Scripts.Models.Health health;
        static bool _isReady = false;

        GameLogic gameLogic;

        public void Ready()
        {
            _isReady = true;
        }
        [System.Serializable]
        public enum Act
        {
            BERTAHAN,
            MENYERANG
        }
        float decNPCHLogic;
        Assets.Game.Scripts.Models.Health healthNPC,healthPlay;
        //val 0 = sedikit, 1 = setengah, 2 = penuh
        int NyawaNPC()
        {
            healthNPC = health;
            return healthNPC.Evaluate(hNPC);
        }
        int NyawaPlayer()
        {
            healthPlay = health;
            return healthPlay.Evaluate(hPlayer);
        }

        string getPercentage()
        {
            float a = healthNPC.GetPowerDecicion();
            float b = healthPlay.GetPowerDecicion();


            var z = a*(hNPC) + b*(hPlayer);
            var x = a + b;
            return (z/x) +" v";
        }
        void InitRule()
        {
            #region Logic Test
            if (GameLogic._intant == null)
            {
                
                //1
                if (NyawaNPC() == 0 && NyawaPlayer() == 0)
                {
                    Debug.Log("Musuh Bertahan Saja "+ getPercentage());
                }
                //2
                else if (NyawaNPC() == 0 && NyawaPlayer() == 1)
                {
                    Debug.Log("Musuh Lebih Bertahan " + getPercentage());
                }
                //3
                else if (NyawaNPC() == 0 && NyawaPlayer() == 2)
                {
                    Debug.Log("Musuh Sangat Bertahan " + getPercentage());
                }
                //4
                else if (NyawaNPC() == 1 && NyawaPlayer() == 0)
                {
                    Debug.Log("Musuh Suka Bertahan " + getPercentage());
                }
                //5
                else if (NyawaNPC() == 1 && NyawaPlayer() == 1)
                {
                    Debug.Log("Musuh Menyerang " + getPercentage());
                }
                //6
                else if (NyawaNPC() == 1 && NyawaPlayer() == 2)
                {
                    Debug.Log("Musuh Lebih Bertahan " + getPercentage());
                }
                //7
                else if (NyawaNPC() == 2 && NyawaPlayer() == 0)
                {
                    Debug.Log("Musuh Senang Hati Menyerang " + getPercentage());
                }
                //8
                else if (NyawaNPC() == 2 && NyawaPlayer() == 1)
                {
                    Debug.Log("Musuh Senang Menyerang " + getPercentage());
                }
                //9
                else if (NyawaNPC() == 2 && NyawaPlayer() == 2)
                {
                    Debug.Log("Musuh Suka Menyerang " + getPercentage());
                }
            }
            #endregion
            #region Logic Live
            else
            {
                //1
                if (NyawaNPC() == 0 && NyawaPlayer() == 0)
                {
                    GameLogic._intant.ActionNPC(Act.BERTAHAN);
                }
                //2
                else if (NyawaNPC() == 0 && NyawaPlayer() == 1)
                {
                    GameLogic._intant.ActionNPC(Act.BERTAHAN);
                }
                //3
                else if (NyawaNPC() == 0 && NyawaPlayer() == 2)
                {
                    GameLogic._intant.ActionNPC(Act.BERTAHAN);
                }
                //4
                else if (NyawaNPC() == 1 && NyawaPlayer() == 0)
                {
                    GameLogic._intant.ActionNPC(Act.BERTAHAN);
                }
                //5
                else if (NyawaNPC() == 1 && NyawaPlayer() == 1)
                {
                    GameLogic._intant.ActionNPC(Act.MENYERANG);
                }
                //6
                else if (NyawaNPC() == 1 && NyawaPlayer() == 2)
                {
                    GameLogic._intant.ActionNPC(Act.BERTAHAN);
                }
                //7
                else if (NyawaNPC() == 2 && NyawaPlayer() == 0)
                {
                    GameLogic._intant.ActionNPC(Act.MENYERANG);
                }
                //8
                else if (NyawaNPC() == 2 && NyawaPlayer() == 1)
                {
                    GameLogic._intant.ActionNPC(Act.MENYERANG);
                }
                //9
                else if (NyawaNPC() == 2 && NyawaPlayer() == 2)
                {
                    GameLogic._intant.ActionNPC(Act.MENYERANG);
                }
                else
                {
                    GameLogic._intant.ActionNPC(Act.MENYERANG);
                }
            }
            #endregion
        }

        void Log(Act act)
        {
            Debug.Log(act);
        }

        private void Start()
        {
            health = GetComponent<Assets.Game.Scripts.Models.Health>();
            
        }

        private void LateUpdate()
        {
            if (test)
            {
                if (_test)
                {
                    TestLogic();
                }
            }
            else
            {
                InitRule();
                if (GameManager._instan != null)
                {
                    hPlayer = GameManager._instan.PlayerHealth();
                    hNPC = GameManager._instan.EnemyHealth();
                }
            }
        }
        [Header("Test Float Value")]
        public float healthMe = 100;
        public float healthEnemy = 100;
        public bool test = false;
        bool _test = false;
        void TestLogic()
        {
            hNPC = healthEnemy/100;
            hPlayer = healthMe/100;
            InitRule();
            
        }
        public void OnClickTestMethode()
        {
            _test = true;
        }
    }
}

