﻿using System;
using UnityEngine;
using Assets.Game.Scripts.Miscellaneous;

namespace Assets.Game.Scripts.Core
{
    public class GameLogic : MonoBehaviour
    {
        public static GameLogic _intant;

        public Animator animator;

        public GameObject espot;
        public UnityEngine.UI.Text roundT;
        public LogicRules logicRules;
        public Miscellaneous.TimeCountBGame counterTimeBeforePlay;
        public Miscellaneous.TimeCountBGame counterTimeGame;
        public Miscellaneous.TimeCountBGame counterRecharge;
        public Miscellaneous.GameOverPopups gameOverPopController;
        [Header("Mesh Player")]
        public MeshRenderer materialGL;
        public MeshRenderer materialGR;
        public SkinnedMeshRenderer materialEnemy;

        public Scripts.Kinect.PlayerController playerController;

        int nowRound = 0;
        public int maxRound = 3;

        bool _RoundPlay;
        bool _RoundChange = true;
        private void Awake()
        {
            _intant = this;
            logicRules = GetComponent<LogicRules>();
        }
        private void Start()
        {
            if (GameManager._instan != null)
            {
                GameManager._instan.SetLogic(this, playerController);
                counterTimeGame.SetTime(GameManager._instan.CounTimeperRound);

                materialGL.material = GameManager._instan.GetCharPlayerData().mat;
                materialGR.material = GameManager._instan.GetCharPlayerData().mat;
                materialEnemy.materials[0] = GameManager._instan.GetCharEnemyData().mat;
            }
            

            counterTimeBeforePlay.GetComponent<Transform>().gameObject.SetActive(true);
            counterTimeBeforePlay.CountStart();
            _RoundPlay = true;
            AudioManager._instan.ForcePlaySFX(0);
            AudioManager._instan.PlayMusic();

        }



        #region Interload
        public void TimeCountBGame_onFinish()
        {
            counterTimeBeforePlay.GetComponent<Transform>().gameObject.SetActive(false);
            
            if (_RoundChange)
            {
                AudioManager._instan.ForcePlaySFX(3);
                nowRound++;
                roundT.text = "Round " + nowRound;
                _RoundChange = false;
                counterTimeGame.CountStart();
            }
            else if(counterTimeGame.GetOnCount()&&counterTimeGame.isPause()&&!onEmptyHealth)
            {
                counterTimeGame.CountResume();
            }
            
        }
        #endregion
        #region Logic
        [System.Serializable]
        public class PlotObject
        {
            //Maximum
            public Vector3 maximum;
            //Minimum
            public Vector3 minimum;

            public float xM()
            {
                return maximum.x;
            }
            public float xm()
            {
                return minimum.x;
            }
            public float yM()
            {
                return maximum.y;
            }
            public float ym()
            {
                return minimum.y;
            }
            public float zM()
            {
                return maximum.z;
            }
            public float zm()
            {
                return minimum.z;
            }
        }
        [Header("Gloves Player")]
        public GameObject gloveRight;
        public GameObject gloveLeft;

        //variable
        public PlotObject def;
        public PlotObject att;

        bool Rbetter = false;
        bool LBetter = false;
        public void ActionNPC(LogicRules.Act act)
        {
            if (counterTimeBeforePlay.GetOnCount()||overgame) return;
            if (onEmptyHealth) return;
            TimeCountBGame_onFinish();
            switch (act)
            {
                case LogicRules.Act.BERTAHAN:
                    if (Calculating(def, gloveRight.transform))
                    {
                        //LBetter = true;
                        //Rbetter = true;
                        DefendingAnim();
                    }
                    break;
                case LogicRules.Act.MENYERANG:
                    if (Calculating(def, gloveRight.transform))
                    {
                        if (CloserDistancePlayerHelper.instance.getDist() > 1)
                        {
                            LBetter = true;
                            Rbetter = true;
                        }
                        else if(CloserDistancePlayerHelper.instance.rightBetter())
                        {
                            Rbetter = true;
                            LBetter = false;
                        }
                        else
                        {
                            Rbetter = false;
                            LBetter = true;
                        }

                        if (CloserDistanceEnemyHelper.instance.getDist() > 1.2f)
                        {
                            DefendingAnim();
                        }
                        else
                        {
                            AttackingAnim();
                        }
                    }
                    break;
            }
        }
        public bool Calculating(PlotObject plot, Transform objectT)
        {
            float distM = Vector3.Distance(plot.maximum, objectT.transform.position);
            float distm = Vector3.Distance(plot.minimum, objectT.transform.position);
            return true;
        }
        #endregion
        #region Animator
        
        public void AttackingAnim()
        {
            initAnimFalse();
            if(LBetter && Rbetter)
            {
                animator.SetBool("AtR", true);
                animator.SetBool("AtL", true);
            }
            else if (LBetter)
            {
                animator.SetBool("AtR", false);
                animator.SetBool("AtL", true);
            }
            else if(Rbetter)
            {
                animator.SetBool("AtR", true);
                animator.SetBool("AtL", false);
            }
            else
            {
                animator.SetBool("AtR", false);
                animator.SetBool("AtL", false);
            }
        }

        public void DefendingAnim() 
        {
            initAnimFalse();
            animator.SetBool("Def", true);
        }
        public void initAnimFalse()
        {
            animator.SetBool("Def", false);
            animator.SetBool("AtR", false);
            animator.SetBool("AtL", false);
            animator.SetBool("Hit", false);

        }
        #endregion

        #region Popup
        bool onEmptyHealth = false;
        Enum.CharacterEnum spot = Enum.CharacterEnum.Player;

        public bool OnNotready()
        {
            return onEmptyHealth;
        }
        public void EmptyHealth(Enum.CharacterEnum e)
        {

            initAnimFalse();
            spot = e; 
            onEmptyHealth = true;
            counterRecharge.GetComponent<Transform>().gameObject.SetActive(true);
            counterRecharge.CountStart();
            AudioManager._instan.ForcePlaySFX(1);
        }
        public void Recarging()
        {
            if (onEmptyHealth)
            {
                counterTimeGame.CountPause();
                
                if (GameManager._instan != null)
                {
                    if(spot == Enum.CharacterEnum.Player)
                    {

                    }
                    else
                    {

                    }
                }
                if (!counterRecharge.GetOnCount())
                {
                    onEmptyHealth = false;
                    if (GameManager._instan != null)
                    {
                        overgame = true;
                        counterTimeGame.CountPause();
                        counterRecharge.GetComponent<Transform>().gameObject.SetActive(false);
                        GameManager._instan.GameKO(spot);
                        gameOverPopController.WinSet(spot, Miscellaneous.GameOverPopups.WinCondition.KO);
                    }
                }
            }
            
        }
        public void GetUpChar()
        {
            AudioManager._instan.StopSFX(1);
            counterRecharge.CountPause();
            counterRecharge.Reset();
            counterRecharge.GetComponent<Transform>().gameObject.SetActive(false);

            counterTimeBeforePlay.GetComponent<Transform>().gameObject.SetActive(true);
            counterTimeBeforePlay.CountStart();

            onEmptyHealth = false;
            AudioManager._instan.ForcePlaySFX(0);

        }
        #endregion
        bool overgame = false;
        private void Update()
        {
            if (!counterTimeGame.GetOnCount())
            {
                if (_RoundChange) return;
                _RoundChange = true;
                if (nowRound >= maxRound)
                {
                    overgame = true;
                    GameManager._instan.GameDraw();
                    if (GameManager._instan.EnemyHealth() > GameManager._instan.PlayerHealth())
                    {
                        gameOverPopController.WinSet(Enum.CharacterEnum.Player, Miscellaneous.GameOverPopups.WinCondition.LostPoint);
                    }
                    else
                    {
                        gameOverPopController.WinSet(Enum.CharacterEnum.Enemy, Miscellaneous.GameOverPopups.WinCondition.LostPoint);
                    }

                }
                counterTimeBeforePlay.GetComponent<Transform>().gameObject.SetActive(true);
                counterTimeBeforePlay.CountStart();
                AudioManager._instan.ForcePlaySFX(0);
            }
            #region Popup
            if (_RoundChange) return;
            Recarging();
            
            #endregion

        }
        private void LateUpdate()
        {
            #region Reposition
            //z
            float distEn = CloserDistanceEnemyHelper.instance.getDist();
            if (onEmptyHealth|| counterTimeBeforePlay.GetOnCount()|| _RoundChange)
            {
                if (distEn >= 1.9f) return;
                Vector3 dpos = new Vector3(espot.transform.position.x, espot.transform.position.y, espot.transform.position.z + (Time.deltaTime));
                espot.transform.position = dpos;
            } else if (distEn > 1)
            {
                if (distEn >= 4 || distEn >=2f) return;
                Vector3 dpos = new Vector3(espot.transform.position.x, espot.transform.position.y, espot.transform.position.z - (Time.deltaTime));
                espot.transform.position = dpos;
            }
            else
            {
                if (distEn <= 0.6f) return;
                Vector3 dpos = new Vector3(espot.transform.position.x, espot.transform.position.y, espot.transform.position.z + (0.2f * Time.deltaTime));
                espot.transform.position = dpos;
            }

            #endregion
        }
    }
}
