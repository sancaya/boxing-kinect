﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.Game.Scripts
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager _instan;

        [Header("Source Musics")]
        public AudioSource music;
        public GameObject sFX;
        private void Awake()
        {
            if (_instan == null)
            {
                _instan = this;
            }
        }

        public void PlayMusic()
        {
            if (music.isPlaying) return;
            music.Play();
        }
        public void StopMusic()
        {
            if (!music.isPlaying) return;
            music.Stop();
        }

        public void MuteMusic()
        {
            if (music.mute == false) music.mute = true;
        }
        public void UnMuteMusic()
        {
            if (music.mute == true) music.mute = false;
        }

        public void PlaySFX(int elmentid)
        {
            sFX.GetComponent<Miscellaneous.SFXHandler>().getObjectAudioSource(elmentid).Play();
        }
        public void ForcePlaySFX(int elmentid)
        {
            sFX.GetComponent<Miscellaneous.SFXHandler>().getObjectAudioSource(elmentid).Play();
        }
        public void StopSFX(int elmentid)
        {
            sFX.GetComponent<Miscellaneous.SFXHandler>().getObjectAudioSource(elmentid).Stop();
        }

        public void MuteSFX()
        {
            foreach (var item in sFX.GetComponent<Miscellaneous.SFXHandler>().getAudioSources())
            {
                item.mute = true;
            }
        }
        public void UnMuteSFX()
        {
            foreach (var item in sFX.GetComponent<Miscellaneous.SFXHandler>().getAudioSources())
            {
                item.mute = false;
            }
        }

    }
}