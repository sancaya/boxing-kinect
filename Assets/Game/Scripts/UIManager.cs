﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class UIManager : MonoBehaviour
    {

        [Header("Att")]
        public Transform zonaSelectPlayer;
        public Transform selectingEnemy;

        private void Awake()
        {
            SetUI();
        }

        private void Start()
        {
            AudioManager._instan.PlayMusic();
        }
        #region SetUI
        void SetUI()
        {
            zonaSelectPlayer.gameObject.SetActive(true);
            selectingEnemy.gameObject.SetActive(false);
        }
        #endregion

        #region Events
        public void EventSelectChar(Models.Character character)
        {
            zonaSelectPlayer.gameObject.SetActive(false);
            selectingEnemy.gameObject.SetActive(true);

            var _characters = GameObject.Find("Scripts").GetComponent<GameManager>().characters;
            int _randSelecting = UnityEngine.Random.Range(0, _characters.Length);

            var sui = selectingEnemy.GetComponent<Miscellaneous.UISelecting>();

            sui.PlayerName.text = _characters[_randSelecting].name;
            sui.PlayerAvt.sprite = _characters[_randSelecting].avatar;
            sui.EnemyName.text = character.name;
            sui.EnemyAvt.sprite = character.avatar;

            var _play = selectingEnemy.Find("Fight").gameObject;
            UnityEngine.EventSystems.EventTrigger evTriger = _play.AddComponent<UnityEngine.EventSystems.EventTrigger>();
            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerClick;
            entry.callback.AddListener((eventData) => {
                Transform _Scripts = GameObject.Find("Scripts").transform;
                _Scripts.GetComponent<GameManager>().PlayGame(character, _characters[_randSelecting]);
            });
            evTriger.triggers.Add(entry);

        }
        #endregion

    }
}
